﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lyra.Models
{
    public class Habit
    {
        public string Category { get; set; }

        public string Name { get; set; }

        public bool Complete { get; set; }
    }
}
