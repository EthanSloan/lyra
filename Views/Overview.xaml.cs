using Lyra.Models;

namespace Lyra.Views;

public partial class Overview : ContentPage
{
    public static List<Habit> HabitList = new();

    public Overview()
    {
		InitializeComponent();

        var habitOne = new Habit
        {
            Name = "First habit",
            Category = "blue",
            Complete = false
        };

        var habitTwo = new Habit
        {
            Name = "Second habit",
            Category = "green",
            Complete = true
        };

        var habitThree = new Habit
        {
            Name = "Third habit",
            Category = "yellow",
            Complete = false
        };

        HabitList.Add(habitOne);
        HabitList.Add(habitTwo);
        HabitList.Add(habitThree);
    }
}